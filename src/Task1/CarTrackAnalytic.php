<?php


namespace App\Task1;

class CarTrackAnalytic
{
    protected Track $track;
    protected Car $car;

    public const SEC_IN_HOUR = 3600;

    /**
     * CarTrackAnalytic constructor.
     * @param Car   $car
     * @param Track $track
     */
    public function __construct (Car $car, Track $track)
    {
        $this->car = $car;
        $this->track = $track;
    }

    /**
     * @return float
     */
    public function calculateMaxDistanceWithoutRefill (): float
    {
        return round(
            $this->car->getFuelTankVolume() / $this->car->getFuelConsumption() * Car::CONSUMPTION_DISTANCE,
            2
        );
    }

    /**
     * @return int
     */
    public function getPitStopQty (): int
    {
        return ceil($this->track->getRaceDistance() / $this->calculateMaxDistanceWithoutRefill());
    }

    /**
     * @return float
     */
    public function calculateNecessaryTime (): float
    {
        return round($this->track->getRaceDistance() / $this->car->getSpeed(), 2) * self::SEC_IN_HOUR
            + $this->car->getPitStopTime() * $this->getPitStopQty();
    }

    /**
     * @return Car
     */
    public function getCar (): Car
    {
        return $this->car;
    }
}
