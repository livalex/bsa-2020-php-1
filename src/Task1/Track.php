<?php

declare(strict_types=1);

namespace App\Task1;

/**
 * Class Track
 * @package App\Task1
 */
class Track
{
    protected float $lapLength;
    protected int $lapsNumber;
    protected array $cars;

    /**
     * Track constructor.
     * @param float $lapLength
     * @param int   $lapsNumber
     */
    public function __construct (float $lapLength, int $lapsNumber)
    {
        if ($lapLength <= 0) {
            throw new \InvalidArgumentException("lapLength value must be greater then 0");
        }
        $this->lapLength = $lapLength;

        if ($lapsNumber <= 0) {
            throw new \InvalidArgumentException("lapsNumber value must be greater then 0");
        }
        $this->lapsNumber = $lapsNumber;

        $this->cars = [];
    }

    /**
     * @return float
     */
    public function getLapLength (): float
    {
        return $this->lapLength;
    }

    /**
     * @return int
     */
    public function getLapsNumber (): int
    {
        return $this->lapsNumber;
    }

    /**
     * @param Car $car
     */
    public function add (Car $car): void
    {
        array_push($this->cars, $car);
    }

    /**
     * @return array
     */
    public function all (): array
    {
        return $this->cars;
    }

    /**
     * @return float
     */
    public function getRaceDistance (): float
    {
        return $this->lapsNumber * $this->lapLength;
    }

    /**
     * @return Car
     */
    public function run (): Car
    {
        if (!count($this->cars)) {
            throw new \LogicException("race can not start with empty track");
        }
        $analysisData = array_map(function (Car $item) {
            return new CarTrackAnalytic($item, $this);
        }, $this->cars);

        $analysisResult = array_map(function (CarTrackAnalytic $analysisItem) {
            return [
                'car'      => $analysisItem->getCar(),
                'raceTime' => $analysisItem->calculateNecessaryTime()
            ];
        }, $analysisData);

        $minRaceTime = $analysisResult[0]['raceTime'];
        $winner = $analysisResult[0]['car'];
        foreach ($analysisResult as $carResult) {
            if ($carResult['raceTime'] < $minRaceTime) {
                $winner = $carResult['car'];
            }
        }

        return $winner;
    }
}
