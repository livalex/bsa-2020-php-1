<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\CarTrackAnalytic;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $content = "<table><thead><th>Name</th><th>Image</th></thead><tbody>";
        $cars = $track->all();

        foreach ($cars as $car) {
            $name = $car->getName();
            $id = $car->getId();
            $raceTime = (new CarTrackAnalytic($car, $track))->calculateNecessaryTime();
            $image = $car->getImage();
            $content .= "<tr><td>{$name}: {$id}, {$raceTime}</td><td><img src=\"{$image}\"></td></tr>";
        }

        $content .= "</tbody></table>";

        return $content;
    }
}
