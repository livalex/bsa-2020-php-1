<?php

declare(strict_types=1);

namespace App\Task2;

/**
 * Class Book
 * @package App\Task2
 */
final class Book
{
    private string $title;
    private int $price;
    private int $pagesNumber;

    /**
     * Book constructor.
     * @param string $title
     * @param int    $price
     * @param int    $pagesNumber
     */
    public function __construct (string $title, int $price, int $pagesNumber)
    {
        if (!$title) {
            throw new \InvalidArgumentException("title value must be not empty line");
        }
        $this->title = $title;

        if ($price <= 0) {
            throw new \InvalidArgumentException("price value must be greater then 0");
        }
        $this->price = $price;

        if ($pagesNumber <= 0) {
            throw new \InvalidArgumentException("pagesNumber value must be greater then 0");
        }
        $this->pagesNumber = $pagesNumber;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}
