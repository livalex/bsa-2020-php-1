<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    protected int $minPagesNumber;
    protected array $libraryBooks;
    protected int $maxPrice;
    protected array $storeBooks;

    /**
     * BooksGenerator constructor.
     * @param int   $minPagesNumber
     * @param array $libraryBooks
     * @param int   $maxPrice
     * @param array $storeBooks
     */
    public function __construct (int $minPagesNumber, array $libraryBooks, int $maxPrice, array $storeBooks)
    {
        if ($minPagesNumber <= 0) {
            throw new \InvalidArgumentException("minPagesNumber value must be greater then 0");
        }
        if ($maxPrice <= 0) {
            throw new \InvalidArgumentException("maxPrice value must be greater then 0");
        }

        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    public function generate (): \Generator
    {
        $filteredBooks = array_merge(
            array_filter($this->libraryBooks, function (Book $book) {
                return $book->getPagesNumber() >= $this->minPagesNumber;
            }),
            array_filter($this->storeBooks, function (Book $book) {
                return $book->getPrice() <= $this->maxPrice;
            })
        );

        foreach ($filteredBooks as $filteredBook) {
            yield $filteredBook;
        }
    }
}
