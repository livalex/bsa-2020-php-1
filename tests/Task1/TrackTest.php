<?php

declare(strict_types=1);

namespace Tests\Task1;

use App\Task1\Car;
use App\Task1\Track;
use PHPUnit\Framework\TestCase;

class TrackTest extends TestCase
{
    public function trackDataProvider (): array
    {
        return [
            [
                2,
                10
            ],
            [
                0,
                10
            ],
            [
                2,
                0
            ],
        ];
    }

    /**
     * @dataProvider trackDataProvider
     */
    public function testCreateTrack (
        float $lapLength,
        int $lapsNumber
    )
    {
        if ($lapLength <= 0) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage("lapLength value must be greater then 0");
        }
        if ($lapsNumber <= 0) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage("lapsNumber value must be greater then 0");
        }
        $track = new Track($lapLength, $lapsNumber);

        $this->assertEquals($lapLength, $track->getLapLength());
        $this->assertEquals($lapsNumber, $track->getLapsNumber());
        $this->assertEquals($lapLength * $lapsNumber, $track->getRaceDistance());
    }
}
