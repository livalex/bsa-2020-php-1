<?php

namespace Tests\Task1;

use App\Task1\Car;
use App\Task1\CarTrackAnalytic;
use App\Task1\Track;
use PHPUnit\Framework\TestCase;

class CarTrackAnalyticTest extends TestCase
{
    private Track $track;
    private Car $car;
    private CarTrackAnalytic $analysis;

    protected function setUp (): void
    {
        parent::setUp();

        $this->track = new Track(20, 50);

        $this->car = new Car(
            1,
            'https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg',
            'BMW',
            250,
            10,
            5,
            15
        );

        $this->analysis = new CarTrackAnalytic($this->car, $this->track);
    }

    public function testCanCalculateMaxDistanceWithoutRefill ()
    {
        $expected = round(
            $this->car->getFuelTankVolume() / $this->car->getFuelConsumption() * Car::CONSUMPTION_DISTANCE,
            2
        );

        $this->assertEquals($expected, $this->analysis->calculateMaxDistanceWithoutRefill());
    }

    public function testCanCalculatePitStopQty ()
    {
        $expected = ceil($this->track->getRaceDistance() / $this->analysis->calculateMaxDistanceWithoutRefill());

        $this->assertEquals($expected, $this->analysis->getPitStopQty());
    }

    public function testCanCalculateRaceTime ()
    {
        $expected = $this->analysis->getPitStopQty() * $this->car->getPitStopTime()
            + round($this->track->getRaceDistance() / $this->car->getSpeed(), 2) * CarTrackAnalytic::SEC_IN_HOUR;

        $this->assertEquals($expected, $this->analysis->calculateNecessaryTime());
    }

    public function carsDataProvider (): array
    {
        return [
            [
                new Car(
                    1,
                    'https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg',
                    'BMW',
                    250,
                    10,
                    5,
                    15
                )
            ], [
                new Car(
                    2,
                    'https://i.pinimg.com/originals/e4/15/83/e41583f55444b931f4ba2f0f8bce1970.jpg',
                    'Tesla',
                    200,
                    5,
                    5.3,
                    18
                )
            ], [
                new Car(
                    3,
                    'https://fordsalomao.com.br/wp-content/uploads/2019/02/1499441577430-1-1024x542-256x256.jpg',
                    'Ford',
                    220,
                    5,
                    6.1,
                    18.5
                ),
            ],
        ];
    }

    /**
     * @dataProvider carsDataProvider
     */
    public function testStart ()
    {
        $maxDistanceWithoutRefill = round(
            $this->car->getFuelTankVolume() / $this->car->getFuelConsumption() * Car::CONSUMPTION_DISTANCE,
            2
        );
        $pitStopQty = ceil($this->track->getRaceDistance() / $maxDistanceWithoutRefill);
        $raceTime = $pitStopQty * $this->car->getPitStopTime()
            + round($this->track->getRaceDistance() / $this->car->getSpeed(), 2) * CarTrackAnalytic::SEC_IN_HOUR;

        $this->assertEquals($this->analysis->calculateNecessaryTime(), $raceTime);
    }
}
